beginning Web 2.0 drupal 6.x theme, created by robin / biboo.net / gazwal.com

---------------
 beginningW2 is a "Web 2.0 style" evolution from my
beginning theme. http://drupal.org/project/beginning
It has to be considered as another blank canvas for theme
developers.
Use beginning as a starting point to help facilitate your theme
development. Simply add background images and you're done !
(find out more)

Same features as beginning theme :

    * 960 px centered fixed width layout (supporting screen resolutions of
      1024 pixels and up)
    * multi-column support (0, 1 or 2 sidebars / auto resizing)
    * source-ordered (the content comes before the left and right sidebars
      in the XHTML source for increased accessibility and SEO)
    * supported features : logo, favicon, name, slogan, mission,
      comment_user_picture, primary_links, secondary_links, search_box
    * W3C standards-compliant : it validates XHTML 1.0 Strict / CSS 2.1
    * cross-browser compatible : works in Firefox, IE6, IE 7, Opera,
      Netscape navigator, Safari
    * it is inspired from deco
      (header area) and Alek
      2.0 (footer regions / both sidebars)

Changes compared with beginning theme :

    * both sidebars take place at the right (seems to be
      fashionable ?)
    * 14 regions 
    (4 regions in the footer for blocks / 230 px width)
    (3 others regions for the Headerblock floating blocks, above Featured region )

Not supported :
-- search box feature in header area (currently displayed in the left sidebar)
------------------

beginning is provided by gazwal.com http://gazwal.com and maintained by robin http://drupal.org/user/76033, a French Drupal developer.

Feel free to contact me : http://drupal.org/user/76033/contact